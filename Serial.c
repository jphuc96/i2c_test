/*
 * Serial.c
 *
 *  Created on: May 1, 2016
 *      Author: root
 */

#include <avr/io.h>
#include <stdint.h>                     // needed for uint8_t
#include <avr/interrupt.h>
#include <Serial.h>

// Macro to determine the baud prescale rate
// See table 22.1 in the Mega datasheet
#define BAUD_PRESCALER (((F_CPU / (BAUDRATE * 16UL))) - 1)

// Initialize for USART
void USART_init(void)
{

// disable global interrupt
	cli();

#if ENABLE_USART0
	// Set the baud rate prescale rate register
	UBRR0 = BAUD_PRESCALER;
	// Enable receiver and transmitter
	UCSR0B = ((1<<RXEN0)|(1<<TXEN0));
	#if USART_INT_MODE
	UCSR0B = ((1<<RXEN0)|(1<<TXEN0)|(1 << RXCIE0));
	#endif
	// Set frame format: 8data, 1 stop bit. See Table 22-7 for details
	UCSR0C = ((0<<USBS0)|(1 << UCSZ01)|(1<<UCSZ00));
#endif

#if ENABLE_USART1
	UBRR1 = BAUD_PRESCALER;
	UCSR1B = ((1<<RXEN1)|(1<<TXEN1));
	#if USART_INT_MODE
	UCSR1B = ((1<<RXEN1)|(1<<TXEN1)|(1 << RXCIE1));
	#endif
	UCSR1C = ((0<<USBS1)|(1 << UCSZ11)|(1<<UCSZ10));
#endif

#if ENABLE_USART2
	UBRR2 = BAUD_PRESCALER;
	UCSR2B = ((1<<RXEN2)|(1<<TXEN2));
	#if USART_INT_MODE
	UCSR2B = ((1<<RXEN2)|(1<<TXEN2)|(1 << RXCIE2));
	#endif
	UCSR2C = ((0<<USBS2)|(1 << UCSZ21)|(1<<UCSZ20));
#endif

#if ENABLE_USART3
	UBRR3 = BAUD_PRESCALER;
	UCSR3B = ((1<<RXEN3)|(1<<TXEN3));
	#if USART_INT_MODE
	UCSR3B = ((1<<RXEN3)|(1<<TXEN3)|(1 << RXCIE3));
	#endif
	UCSR3C = ((0<<USBS3)|(1 << UCSZ31)|(1<<UCSZ30));
#endif

	// enable global interrupt
	sei();
}

void USART_send( volatile char data, uint8_t usart_num)
{

#if ENABLE_USART0
	if (usart_num == 0)
	{
		//while the transmit buffer is not empty loop
		while((!(UCSR0A & (1<<UDRE0))) );
		// when the buffer is empty write data to the transmitted
		// and there is no timeout
		UDR0 = data;

	}
#endif

#if ENABLE_USART1
	if (usart_num == 1)
	{
		//while the transmit buffer is not empty loop
		while((!(UCSR1A & (1<<UDRE1))) );
		//when the buffer is empty write data to the transmitted
		UDR1 = data;

	}
#endif

#if ENABLE_USART2
	if (usart_num == 2)
	{
		while((!(UCSR2A & (1<<UDRE2))) );
		//when the buffer is empty write data to the transmitted
		UDR2 = data;

	}
#endif

#if ENABLE_USART3
	if (usart_num == 3)
	{
		//while the transmit buffer is not empty loop
		while((!(UCSR3A & (1<<UDRE3))) );
		//when the buffer is empty write data to the transmitted
		UDR3 = data;

	}
#endif
}


void USART_putstring(volatile char* StringPtr, uint8_t usart_num)
// sends the characters from the string one at a time to the USART
{
	while(*StringPtr != 0x00)
	{
		USART_send(*StringPtr,usart_num);
		StringPtr++;
	}
}
