/*
test I2C DS1037
*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <>Serial.h>
#include <stdlib.h>

void init_i2c()
	{
		TWSR = 0X00;
		TWBR = 0X47;
		TWCR = (1<<TWEN);
	}

unsigned char read_i2c()
	{
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR & (1<<TWINT)));
		return TWDR;
	}

void write_i2c(unsigned char ch)
	{
		TWDR = ch;
		TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
		while(!(TWCR&(1<<TWINT)));
	}

void start()
	{
		TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
		while((TWCR &(1<<TWINT))==0);
	}

void stop()
	{
		TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
		_delay_ms(1);
	}

void rtc_write(char dev_addr,char dev_loc,char dev_data)
	{
		start();
		write_i2c(dev_addr);
		write_i2c(dev_loc);
		write_i2c(dev_data);
		stop();
		_delay_ms(10);
	}

unsigned char rtc_read(char dev_addr,char dev_loc)
	{
		char ch;

		start();

		write_i2c(dev_addr);
		write_i2c(dev_loc);

		start();
		write_i2c(dev_addr|0x01);
		ch = read_i2c();

		stop();
		return ch;
	}


void disp_time_date()
	{
		char ch;
        int usart_port = 0;

		USART_send(32,usart_port);
		USART_send(32,usart_port);

		USART_putstring("Time: ",usart_port);

		ch = rtc_read(0xd0 , 0x02);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);
		USART_send('-',usart_port);

		ch = rtc_read(0xd0 , 0x01);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);
		USART_send('-',usart_port);

		ch = rtc_read(0xd0 , 0x00);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);

		USART_send(32,usart_port);
		USART_send(32,usart_port);

		USART_putstring("Date: ",usart_port);

		ch = rtc_read(0xd0 , 0x04);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);
		USART_send('-',usart_port);

		ch = rtc_read(0xd0 , 0x05);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);
		USART_send('-',usart_port);

		ch = rtc_read(0xd0 , 0x06);
		USART_send(ch/16+48,usart_port);
		USART_send(ch%16+48,usart_port);

		USART_putstring("\r\n",usart_port);
}

void adc_init(void)
{
	ADCSRA |= ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)); //16MHz/128 = 125kHz the ADC reference clock
	ADMUX  |= (1<<REFS0);   //Set Voltage reference to Avcc (5v)
	ADCSRA |= (1<<ADEN);    //Turn on ADC
	ADCSRA |= (1<<ADSC);    //Do an initial conversion
}

uint16_t read_adc(uint8_t channel)
{
	ADMUX &= 0xE0;           //Clear bits MUX0-4
	ADMUX |= channel&0x07;   //Defines the new ADC channel to be read by setting bits MUX0-2
	ADCSRB = channel&(1<<3); //Set MUX5
	ADCSRA |= (1<<ADSC);      //Starts a new conversion
	while(ADCSRA & (1<<ADSC));  //Wait until the conversion is done
	return ADCW;
}         //Returns the ADC value of the chosen channel

int main(void)
{


	DDRC =0XFF; DDRD =0XFF;
	DDRG = 0xFF;
	PORTG = 0b100100;

	init_i2c();
	//init_serial();
	USART_init();
	adc_init();

//	rtc_write(0xd0,0x00,0x00);
//	rtc_write(0xd0,0x01,0x55);
//	rtc_write(0xd0,0x02,0x14);
//
//	rtc_write(0xd0,0x04,0x11);
//	rtc_write(0xd0,0x05,0x08);
//	rtc_write(0xd0,0x06,0x16);

//	_delay_ms(1000);

	while(1)
	{
		//disp_time_date();
	}

	return 1;
}

void get_time(uint8_t *time)
{
	time[0] = rtc_read(0xd0 , 0x02);//hh
	time[1] = rtc_read(0xd0 , 0x01);//mm
	time[2] = rtc_read(0xd0 , 0x00);//ss

	time[3] = rtc_read(0xd0 , 0x04);//dd
	time[4] = rtc_read(0xd0 , 0x05);//mm
	time[5] = rtc_read(0xd0 , 0x06);//yy

}

uint16_t bcd2i(uint16_t bcd) {
	uint16_t decimalMultiplier = 1;
	uint16_t digit;
	uint16_t i = 0;
    while (bcd > 0) {
        digit = bcd & 0xF;
        i += digit * decimalMultiplier;
        decimalMultiplier *= 10;
        bcd >>= 4;
    }
    return i;
}

uint16_t i2bcd(uint16_t i) {
	uint16_t binaryShift = 0;
	uint16_t digit;
	uint16_t bcd = 0;
    while (i > 0) {
        digit = i % 10;
        bcd += (digit << binaryShift);
        binaryShift += 4;
        i /= 10;
    }
    return bcd;
}


ISR (USART0_RX_vect)
{
	unsigned char ReceivedChar;
	uint16_t val_ADC;
    char str[20]="Hello";

	ReceivedChar = UDR0;
    USART_send(ReceivedChar,0);
    disp_time_date();

    val_ADC = read_adc(0);
    utoa(val_ADC,str,10);
    USART_putstring(str,0);
    USART_putstring(" \r\n",0);
}

