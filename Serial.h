/*
 * Serial.h
 *
 *  Created on: Apr 27, 2016
 *      Author: root
 */

#ifndef SERIAL_H_
#define SERIAL_H_

// CPU Freq
//#define F_CPU 11059200UL
#define F_CPU 16000000UL

// Enable USART
#define ENABLE_USART0 1
#define ENABLE_USART1 0
#define ENABLE_USART2 0
#define ENABLE_USART3 0

// choose USART Mode
// 0 -> polling
// 1 -> interrupt
#define USART_INT_MODE 1

// Baurate
#define BAUDRATE 9600
#define USART_TIMEOUT 1535

// function prototype
void USART_init(void);

/*Send a char using UART with usart_num = 0 or 1 */
void USART_send( volatile char data, uint8_t usart_num);
/*Send a string using UART with usart_num = 0 or 1 */
void USART_putstring(volatile char* StringPtr, uint8_t usart_num);

#endif /* SERIAL_H_ */
